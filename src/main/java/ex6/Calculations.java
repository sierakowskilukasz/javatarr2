package ex6;

import java.util.List;

public class Calculations {

    /**
     * Find maximum element in provided list
     *
     * @param elements List of elements
     * @return Maximal value in provided list
     */
    public Integer findMax(List<Integer> elements) {
        Integer maxValue = Integer.MIN_VALUE;
        for (Integer element : elements) {
            if (element > maxValue) {
                maxValue = element;
            }
        }
        return maxValue;
    }

    /**
     * Find minimum element in provided list
     *
     * @param elements List of elements
     * @return Minimal value in provided list
     */
    public Integer findMin(List<Integer> elements) {
        Integer minValue = Integer.MAX_VALUE;
        for (Integer element : elements) {
            if (element < minValue) {
                minValue = element;
            }
        }
        return minValue;
    }

    public Float findArithmeticAvg(List<Integer> elements) {
        Integer sum = 0;
        for (Integer element : elements) {
            sum += element; // sum = sum + element;
        }
        return (float) sum / elements.size();
    }

    public Float findGeometricAvg(List<Integer> elements) {
        Integer product = 1;
        for (Integer element : elements) {
            product *= element; // product = product * element;
        }
        return (float) Math.pow(Math.E, Math.log(product) / elements.size());
    }
}
