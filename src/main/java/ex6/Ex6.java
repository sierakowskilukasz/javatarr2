package ex6;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Przygotuj aplikację, która umożliwia wprowadzenie 10 liczb przez użytkownika.
 * Spośród wprowadzonych liczb program wyszukuje najmniejszą i największą liczbę,
 * oblicza średnią arytmetyczną, średnią geometryczną oraz wyświetla obliczone
 * wartości użytkownikowi.
 */
public class Ex6 {
    private static Calculations calculations = new Calculations();

    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();

        // Pobierz 10 liczb od użytkownika
        numbers = getNumbersFromUser(10);

        // Znajdź liczbę maksymalną
        Integer maxValue = calculations.findMax(numbers);

        // Znajdź liczbę minimalną
        Integer minValue = calculations.findMin(numbers);

        // Oblicz średnią arytmetyczną
        Float arithmeticAvg = calculations.findArithmeticAvg(numbers);

        // Oblicz średnią geometryczną
        Float geometricAvg = calculations.findGeometricAvg(numbers);

        // Wyświetl wynik
        displayResult(minValue, maxValue, arithmeticAvg, geometricAvg);
    }

    private static List<Integer> getNumbersFromUser(Integer amountOfNumbers) {
        List<Integer> result = new ArrayList<>();
        System.out.println("Wprowadź liczby całkowite");
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < amountOfNumbers; i++) {
            System.out.print("Liczba " + (i + 1) + ": ");
            result.add(scanner.nextInt());
        }
        return result;
    }

    private static void displayResult(Integer minValue, Integer maxValue, Float arithmeticAvg,
                                      Float geometricAvg) {
        System.out.println("--== Wynik działania programu ==--");
        System.out.println("Liczba maksymalna: " + maxValue);
        System.out.println("Liczba minimalna: " + minValue);
        System.out.println("Średnia arytmetyczna: " + arithmeticAvg);
        System.out.println("Średnia geometryczna: " + geometricAvg);
    }
}
