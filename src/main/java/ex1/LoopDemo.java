package ex1;

public class LoopDemo {
    public static void main(String[] args) throws InterruptedException {
        // forLoop();
        // whileLoop();
        doWhileLoop();
        //        int counter1 = 0;
        //        int counter2 = 0;
        //        int z1 = counter1++;
        //        int z2 = ++counter2;
    }

    public static void forLoop() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            Thread.sleep(1000);
            System.out.println("Liczba: " + i);
        }
    }

    public static void whileLoop() {
        int counter = 0;
        while (counter < 10) {
            System.out.println("Liczba: " + counter);
            counter++;
        }
    }

    public static void doWhileLoop() {
        int counter = 0;
        do {
            System.out.println("Liczba: " + counter);
            counter++;
        } while (counter < 10);
    }
}
