package ex1;

import java.util.Scanner;

public class ConditionsDemo {
    public static void main(String[] args) {
        System.out.print("Podaj kolor: ");
        Scanner scanner = new Scanner(System.in);
        String color = scanner.nextLine();

        if ("red".equals(color)) {
            System.out.println("Podałeś kolor czerwony");
        } else if ("blue".equals(color)) {
            System.out.println("Podałeś kolor niebieski");
        } else if ("green".equals(color)) {
            System.out.println("Podałeś kolor zielony");
        } else if ("yellow".equals(color)) {
            System.out.println("Podałeś kolor żółty");
        } else {
            System.out.println("Nie znam takiego koloru");
        }
    }
}
