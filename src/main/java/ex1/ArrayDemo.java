package ex1;

import java.util.Arrays;

public class ArrayDemo {
    public static void main(String[] args) {
        int[] intArray = new int[10];
        intArray[0] = 123;
        intArray[1] = 233;
        intArray[2] = 11;
        intArray[9] = 999;

        System.out.println("Element tablicy: " + intArray[0]);
        System.out.println("Element tablicy: " + intArray[1]);
        System.out.println("Element tablicy: " + intArray[2]);
        System.out.println("Element tablicy: " + intArray[9]);
        System.out.println("Element tablica: " + intArray[3]);

        Integer[] intArray2 = new Integer[10];
        intArray2[0] = 100;
        System.out.println("Element tablicy: " + intArray2[0]);
        System.out.println("Element tablicy: " + intArray2[1]);
        int arrayLength = intArray2.length;
        System.out.println("Długość tablicy intArray2 wynosi: " + arrayLength);
        System.out.println("Długość tablicy intArray2 wynosi: " + intArray2.length);
        intArray2[0] = intArray2[0] + 2;
        System.out.println("Element tablicy: " + intArray2[0]);
        intArray2[0] += 2;
        System.out.println("Element tablicy: " + intArray2[0]);

        // Tablica wielowymiarowa
        int[][] intArray3 = new int[10][10];                                    // Deklaracja i inicjalizacja
        intArray3[0][0] = 1010;                                                 // Przypisanie wartości
        intArray3[9][9] = 123456;
        System.out.println("Element tablicy: " + intArray3[0][0]);              // Wyświetlenie wartości

        int[] sumArray = new int[3];
        sumArray[0] = 1;
        sumArray[1] = 55;
        sumArray[2] = sumArray[0] + sumArray[1];
        System.out.println("Suma elementów w tablicy wynosi: " + sumArray[2]);  // Suma elementów w tablicy wynosi: 56
        System.out.println(sumArray);                                           // [I@1b6d3586
        System.out.println(Arrays.toString(sumArray));                          //[1, 55, 56]

        // Kopiowanie talibcy
        int[] arrayFirst = new int[] { 1, 2, 3 };
        int[] arraySecond = new int[10];
        // arraySecond[0] = arrayFirst[0];
        // arraySecond[1] = arrayFirst[1];
        // arraySecond[2] = arrayFirst[2];
        arraySecond = Arrays.copyOfRange(arrayFirst, 0, 3);
        System.out.println(Arrays.toString(arraySecond));

        int[] arrayThird = new int[10];
        System.arraycopy(arrayFirst, 0, arrayThird, 0, arrayFirst.length);
        System.out.println(Arrays.toString(arrayThird));

        Integer[] arraySrc = new Integer[] { 1, 2, 3 };
        Integer[] arrayFour = new Integer[10];
        System.arraycopy(arraySrc, 0, arrayFour, 0, arraySrc.length);
        System.out.println(Arrays.toString(arrayFour));
    }
}
