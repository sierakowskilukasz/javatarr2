package ex1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsoleCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); // Inicjalizacja czytania z konsoli

        int intFirstNumber = readNumber(1);

        int intSecondNumber = readNumber(2);

        int sumNumbers = intFirstNumber + intSecondNumber;
        System.out.println("Suma wprowadzonych liczb wynosi: " + sumNumbers);
    }

    private static int readNumber(int counter) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Wprowadź " + counter + " liczbę: ");
        String number = scanner.nextLine();
        int intNumber = 0;
        try {
            intNumber = Integer.parseInt(number);
        } catch (NumberFormatException exception) {
            System.out.println("Wprowadzona liczba " + number + " nie jest liczbą całkowitą");
        }
        Integer[] ssss = new Integer[2];
        List<Integer> sss = new ArrayList<>();
        sss.toArray(ssss);
        return intNumber;
    }
}
