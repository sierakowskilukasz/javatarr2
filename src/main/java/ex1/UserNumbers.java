package ex1;

import java.util.Scanner;

public class UserNumbers {
    public static void main(String[] args) {

        // 1. Pobrać od użytkownika ilość liczb do wprowadzenia. - Scanner
        // 2. Pobrać n liczb. - Scnanner + pętla for
        // 3. Zapamiętać te liczby. - Tablica n-elementowa jednowymiarowa
        // 4. Wyświetlić liczby które użytkownik wprowadził. - System.out + while

        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj ilość liczb do wprowadzenia: ");
        int amountOfNumbers = scanner.nextInt();

        int[] numbers = new int[amountOfNumbers];

        for (int i = 0; i < amountOfNumbers; i++) {
            System.out.print("Podaj liczbę " + (i + 1) + ": ");
            int number = scanner.nextInt();
            numbers[i] = number;                 // numbers[i] = scanner.nextInt();
        }

        int counter = 0;
        System.out.println("Twoje liczby to");
        while (counter < numbers.length) {
            System.out.print(numbers[counter] + " ");
            counter++;
        }
    }
}
