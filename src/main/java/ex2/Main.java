package ex2;

import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) {
        Person person = new Person();
        System.out.println(person.toString());

        Person person2 = new Person("Lukasz", "Sierakowski");
        System.out.println(person2.toString());

        System.out.println("Imię osoby to: " + person2.getName());
        System.out.println("Nazwisko osoby to: " + person2.getSurname());
        person2.setName("Kamil");
        System.out.println("Imię osoby to: " + person2.getName());

        Person person3 = new Person("Łukasz", "Sierakowski", LocalDateTime.now(), "green", 'M');
        System.out.println(person3.toString());

        PersonL personL = new PersonL();
        System.out.println(personL.toString());
        personL.display();
        personL.calculateBMI();
        personL.getHeight();
        personL.getWeight();
        String t = PersonL.NATIONALITY;
    }
}
