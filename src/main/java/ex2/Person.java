package ex2;

import java.time.LocalDateTime;

public class Person {
    private String name;
    private String surname;
    private LocalDateTime dateOfBirth;
    private String eyeColor;
    private char sex;

    // Constructors
    public Person() {
    }

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Person(String name, String surname, LocalDateTime dateOfBirth, String eyeColor, char sex) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.eyeColor = eyeColor;
        this.sex = sex;
    }


    // Getters and setters
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDateTime getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    // Formatting
    @Override
    public String toString() {
        return String.format("Imię: %s Nazwisko: %s", name, surname);
    }
}
