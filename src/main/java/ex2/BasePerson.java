package ex2;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BasePerson {
    private int height;
    private int weight;

    public abstract void display();

    public float calculateBMI() {
        return weight / (height / 100 * height / 100);
    }
}
