package ex2;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PersonL extends BasePerson implements PersonIdentity {
    public static final String NATIONALITY = "PL";
    private String pesel;
    private String name;
    private String surname;
    private LocalDateTime dateOfBirth;
    private String eyeColor;
    private char sex;

    @Override
    public void display() {
        this.toString();
    }

    @Override
    public String getIdentity() {
        return pesel;
    }
}
