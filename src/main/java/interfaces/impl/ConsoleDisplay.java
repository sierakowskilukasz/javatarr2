package interfaces.impl;

import interfaces.Display;

/**
 * This class display text on console.
 */
public class ConsoleDisplay implements Display {

    @Override
    public void displayText(String text) {
        System.out.println("Tekst jest wyświetlany na konsoli");
    }
}
