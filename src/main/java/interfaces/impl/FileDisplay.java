package interfaces.impl;

import interfaces.Display;

/**
 * This class write text to file.
 */
public class FileDisplay implements Display {

    @Override
    public void displayText(String text) {
        System.out.println("Tekst jest zapisywany do pliku");
    }
}
