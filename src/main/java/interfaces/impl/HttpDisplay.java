package interfaces.impl;

import interfaces.Display;

/**
 * This class display text on web.
 */
public class HttpDisplay implements Display {

    @Override
    public void displayText(String text) {
        System.out.println("Tekst jest wyświetlany na stronie internetowej :)");
    }
}
