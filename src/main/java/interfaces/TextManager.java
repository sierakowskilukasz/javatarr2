package interfaces;

import lombok.AllArgsConstructor;

/**
 * This class allows to display text on different media.
 */
@AllArgsConstructor
public class TextManager {

    private Display display;

    void displayText(String text) {
        display.displayText(text);
    }
}
