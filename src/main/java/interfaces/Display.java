package interfaces;

public interface Display {

    void displayText(String text);
}
