package interfaces;

import interfaces.impl.ConsoleDisplay;
import interfaces.impl.FileDisplay;
import interfaces.impl.HttpDisplay;

public class Main {
    public static void main(String[] args) {
        final String text = "Zażółć gęślą jaźń";

        TextManager textManagerFile = new TextManager(new FileDisplay());
        TextManager textManagerConsole = new TextManager(new ConsoleDisplay());
        TextManager textManagerHttp = new TextManager(new HttpDisplay());

        textManagerFile.displayText(text);
        textManagerConsole.displayText(text);
        textManagerHttp.displayText(text);
    }
}
