package ex3;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;

public class Collections {
    public static void main(String[] args) {
        List<String> lista = new ArrayList<>();
        // Dodaj element do listy
        lista.add("Łukasz");
        lista.add("Marcin");
        lista.add("Kamil");
        lista.add("Ewa");
        // Pobierz 0 elementy z listy
        String element = lista.get(0);
        // Ilość elementów w liście
        int size = lista.size();
        // Usuwanie elemntu z listy
        lista.remove(0);
        // Sprawdzenie na której pozycji znajduje się określony element
        int x = lista.indexOf("Łukasz");
        // Sprawdzenie czy element znajduje się na liście
        boolean exists = lista.contains("Łukasz");

        int counter = 0;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).equals("Łukasz")) {
                counter++;
            }
        }
        // lista.stream().filter(el-> el.equals("Łukasz")).count();

        // Pobranie pierwsze elementu
        String firstElement = lista.get(0);

        // Pobranie ostatniego elementu
        String lastElement = lista.get(lista.size() - 1);

        // Generownie liczby pseudolosowej
        Random random = new Random();
        // Liczba od 0 do 10 (1-10)
        int number1 = random.nextInt(11);
        // Liczba 0d 10 do 20 (10-20)
        int number2 = random.nextInt(11) + 10;

        // Kolejka
        // Inicjalizacja kolejki
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        Queue<Integer> queueSecond = new PriorityQueue<>();

        // Dodanie elementu do kolejki
        queue.add(11);

        // Pobranie elementu z kolejki i pozostawienie
        Integer value1 = queue.peek();

        // Pobranie elementu z kolejki i usunięcie
        Integer value2 = queue.poll();

        // Ilość elementów w kolejce
        Integer numberOfElementsInQueue = queue.size();
    }
}
