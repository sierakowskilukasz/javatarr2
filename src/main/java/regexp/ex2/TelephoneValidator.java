package regexp.ex2;

import java.util.regex.Pattern;

public class TelephoneValidator {
    private final String pattern = "^(\\+\\d{2})?((\\d{9})|(\\d{3} \\d{3} \\d{3})|(\\d{3}-\\d{3}-\\d{3}))$";

    public boolean validate(String telephone) {
        return Pattern.matches(pattern, telephone);
    }
}
