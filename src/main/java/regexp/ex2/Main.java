package regexp.ex2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.print("Wprowadź numer telefonu: ");
        Scanner scanner = new Scanner(System.in);

        String telephoneNumber = scanner.nextLine();
        TelephoneValidator validator = new TelephoneValidator();
        boolean isValid = validator.validate(telephoneNumber);
        if (isValid) {
            System.out.println("Numer telefonu jest poprawny");
        } else {
            System.out.println("Numer telefonu jest niepoprawny");
        }
    }
}
