package ex4;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {

    @Test
    public void shouldAddTwoNumbersCorrectly() {
        Calculator calculator = new Calculator();
        Integer result = calculator.sum(5, 10);
        Integer expectedResult = 15;
        Assert.assertEquals(expectedResult, result);
    }
}
