package regexp.ex1;

import org.junit.Assert;
import org.junit.Test;

public class TelephoneValidatorTest {
    TelephoneValidator validator = new TelephoneValidator();

    @Test
    public void shouldValidateNumber() {
        Assert.assertTrue(validator.validate("505879357"));
    }

    @Test
    public void shouldValidateNumberWithSpaces() {
        Assert.assertTrue(validator.validate("505 879 357"));
    }

    @Test
    public void shouldValidateNumberWithDashes() {
        Assert.assertTrue(validator.validate("505-879-357"));
    }

    @Test
    public void shouldNotValidateNumberWithSpaces() {
        Assert.assertFalse(validator.validate("50 58 79 35 7"));
    }

    @Test
    public void shouldNotValidateToShortNumber() {
        Assert.assertFalse(validator.validate("5058794"));
    }

    @Test
    public void shouldNotValidateToLongNumber() {
        Assert.assertFalse(validator.validate("44505879357"));
    }
}