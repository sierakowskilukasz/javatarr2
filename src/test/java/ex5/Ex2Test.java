package ex5;

import org.junit.Assert;
import org.junit.Test;

public class Ex2Test {

    @Test
    public void shouldFormatArrayCorrectly() {
        int[] array = new int[] { 1, 5, 10, 55, 33 };
        Ex2 ex2 = new Ex2();
        String actual = ex2.formatArray(array);
        String expected = "{ 1, 5, 10, 55, 33 }";
        Assert.assertEquals(expected, actual);
    }
}
class Tabli {

    public static void main(String[] args) {
        int[] tablica = new int[10];

        for (int i = 1; i <= 10; i++)
            tablica[i] = i;

        int zmienna = tablica[3];

        for (int i = 0; i < 10; i++)
            System.out.println("tab["+ i +"]= " + tablica[i]);
    }
}